package main;

import javax.sound.sampled.SourceDataLine;

import entity.BoPhim;
import entity.HangSanXuat;
import entity.Ngay;

public class main {

    public static void main(String[] args) {
        Ngay ngay1 = new Ngay(15, 05, 2022);
        Ngay ngay2 = new Ngay(11, 01, 2022);
        Ngay ngay3 = new Ngay(18, 02, 2022);

        HangSanXuat hangSanXuat1 = new HangSanXuat("hang a", "viet nam");
        HangSanXuat hangSanXuat2 = new HangSanXuat("hang b", "nhat");
        HangSanXuat hangSanXuat3 = new HangSanXuat("hang c", "my");

        BoPhim phim1 = new BoPhim("phim A", 2019, hangSanXuat1, 50000, ngay1);
        BoPhim phim2 = new BoPhim("phim B", 2018, hangSanXuat2, 150000, ngay3);
        BoPhim phim3 = new BoPhim("phim C", 2010, hangSanXuat3, 250000, ngay2);

        System.out.println("ss gia 1 re hon 2: " + phim1.kiemTraGiaVe(phim2));
        System.out.println("ten hang san xuat: "+ phim3.layTenHangSanXuat());
        System.out.println("giam 10%: " + phim2.layGiaSauKhuyenMai(10));
    }
}